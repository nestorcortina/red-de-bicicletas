const mongoose = require('mongoose');
require('dotenv').config({ path: 'variables.env' });

const Bicicleta = require('../../models/bicicleta');
const request = require('request');

const server = require('../../bin/www');

const base_url = 'http://127.0.0.1:3000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeEach((done) => {
        mongoose.connect(process.env.DBTEST, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        mongoose.connection.on('error', (error) => {
            console.log(error);
        });
        mongoose.connection.once('open', () => {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET bicicletas', () => {
        it('Status 200', (done) => {
            request.get(base_url, function (error, response, body) {
                const result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });

        });
    });

    describe('POST bicicletas', () => {
        it('Status 200', (done) => {
            const headers = { 'content-type': 'application/json' };
            const aBici = '{"id": 10, "color": "rojo", "modelo": "campo", "lat": -34, "lng": -54}';
            request.post({
                headers,
                url: `${base_url}/create`,
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                const bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe('verde');
                expect(bici.ubicacion[0]).toEqual(-34)
                expect(bici.ubicacion[1]).toEqual(-54)
                done();
            });
        });
    });

});