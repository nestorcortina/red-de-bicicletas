const mongoose = require('mongoose');
require('dotenv').config({ path: 'variables.env' });

const Bicicleta = require('../../models/bicicleta');

describe('Testing bicicletas', () => {
    beforeEach((done) => {
        mongoose.connect(process.env.DBTEST, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        mongoose.connection.on('error', (error) => {
            console.log(error);
        });
        mongoose.connection.once('open', () => {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta', () => {
            const bici = Bicicleta.createInstance(1, 'verde', 'urbana', [34.5, -54.3]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(34.5)
            expect(bici.ubicacion[1]).toEqual(-54.3)
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza vacia', (done) => {
            Bicicleta.allBicis((err, bicis) => {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Agrega solo una bici', (done) => {
            const aBici = new Bicicleta({ code: 1, color: 'verde', modelo: 'campo' });
            Bicicleta.add(aBici, (err, newBici) => {
                if (err) console.log(err);
                Bicicleta.allBicis((err, bicis) => {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis((err, bicis) => {
                expect(bicis.length).toBe(0);

                const aBici = new Bicicleta({ code: 1, color: 'verde', modelo: 'campo' });
                Bicicleta.add(aBici, (err, newBici) => {
                    if (err) console.log(err);

                    const aBici2 = new Bicicleta({ code: 2, color: 'roja', modelo: 'campo' });
                    Bicicleta.add(aBici2, (err, newBici) => {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1,(err, targetBici) => {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });

                });
            });
        });
    });

});