const map = L.map('mapid').setView([10.9895582, -74.7904575], 15);
const enlaceMapa = '<a href="http://openstreetmap.org">OpenStreetMap</a>';
L.tileLayer(
    'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; ' + enlaceMapa + ' Contributors',
    maxZoom: 18,
}).addTo(map);

const PopUpPortal = L.popup().setContent(`<p>Nombre: Portal del prado</p>`);
const PopUpItsa = L.popup().setContent(`<p>Nombre: Itsa</p>`);

//L.marker([10.9872059, -74.7821026]).bindPopup(PopUpItsa).addTo(map);
//L.marker([10.9874587, -74.78339]).bindPopup(PopUpPortal).addTo(map);

$.ajax({
    data: "json",
    url: "api/bicicletas",
    success: function (result) {
        console.log(result);
        result.bicicletas.forEach((bici) => {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(map);
        })
    }
})