const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const moment = require('moment');
const Reserva = require('./reserva');

const reservaShema = new mongoose.Schema({
    desde: Date,
    hasta: Date,
    bicicleta: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta'
    },
    usuario: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Usuario'
    }
});

reservaShema.methods.diasDeReserva = function () {
    return moment(this.hasta).diff(moment(this.desde), 'days') + 1;
}

module.exports = mongoose.model('Reserva', reservaShema);