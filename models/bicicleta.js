const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const bicicletaShema = new mongoose.Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true }
    }
});

bicicletaShema.statics.createInstance = function (code, color, modelo, ubicacion) {
    return new this({
        code,
        color,
        modelo,
        ubicacion
    });
}

bicicletaShema.methods.toString = function () {
    return `id: ${this.id} | color: ${this.color}`;
}

bicicletaShema.statics.allBicis = function (cb) {
    return this.find({}, cb);
}

bicicletaShema.statics.add = function (aBici, cb) {
    this.create(aBici, cb);
}

bicicletaShema.statics.findByCode = function (aCode, cb) {
    return this.findOne({ code: aCode }, cb);
}

bicicletaShema.statics.removeByCode = function (aCode, cb) {
    return this.deleteOne({ code: aCode }, cb);
}

module.exports = mongoose.model('Bicicleta', bicicletaShema);