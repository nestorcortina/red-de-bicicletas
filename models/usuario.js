const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Reserva = require('./reserva');

const usuarioShema = new mongoose.Schema({
    nombre: String
});

usuarioShema.methods.reservar = function (biciId, desde, hasta, cb) {
    const reserva = new Reserva({ usuario: this._id, bicicleta: biciId, desde, hasta });
    console.log(reserva);
    reserva.save(cb);
}

module.exports = mongoose.model('Usuario', usuarioShema);