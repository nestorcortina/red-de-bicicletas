const Usuario = require('../../models/usuario');

// Obtener todos los usuarios
exports.usuariosList = async (req, res) => {
    await Usuario.find();
    res.status(200).json({
        usuarios
    });
}

// Crear usuario
exports.usuarioCreatePost = async (req, res) => {
    const usuario = await new Usuario({ nombre: req.body.nombre });
    usuario.save();
    res.status(200).json({
        usuario
    });
}

// Crear reserva
exports.usuarioReserva = async (req, res) => {
    const usuario = await Usuario.findById(req.body.id);
    console.log(usuario);
    usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta);
    res.status(200).send('Se creo la reserva');
}