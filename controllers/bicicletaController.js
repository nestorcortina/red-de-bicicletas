const Bicicleta = require('../models/bicicleta');

exports.bicicletaList = (req, res) => {
    res.render('bicicletas/index', {
        bicis: Bicicleta.allBicis
    })
}

// Form crear bicicleta
exports.bicicletaCreateGet = (req, res) => {
    res.render('bicicletas/create')
}
// Crear bicicleta
exports.bicicletaCreatePost = (req, res) => {
    const bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng]
    Bicicleta.add(bici);

    res.redirect('/bicicletas');
}

// Form update bicicleta
exports.bicicletaUpdateGet = (req, res) => {
    const bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', {
        bici
    })
}
// Update bicicleta
exports.bicicletaUpdatePost = (req, res) => {
    const bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng]

    res.redirect('/bicicletas');
}

// Delete bicicleta
exports.bicicletaDelete = (req, res) => {
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}