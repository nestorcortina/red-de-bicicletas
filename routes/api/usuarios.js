var express = require('express');
var router = express.Router();

const usuarioController = require('../../controllers/api/usuarioControllerAPI');

router.get('/', usuarioController.usuariosList);
// Create
router.post('/create', usuarioController.usuarioCreatePost);
/// Reserva
router.post('/reservar', usuarioController.usuarioReserva);

module.exports = router;